package org.opentele.server.model

import org.opentele.server.core.model.ConferenceMeasurementDraftType

import java.util.regex.Pattern

class ConferenceUrineCombiMeasurementDraft extends ConferenceMeasurementDraft {

    String protein
    String urineGlucose
    String urineBlood
    String urineNitrite
    String urineLeukocytes

    static constraints = {
        protein(nullable: true)
        urineGlucose(nullable: true)
        urineBlood(nullable: true)
        urineNitrite(nullable: true)
        urineLeukocytes(nullable: true)
    }

    static Map<String, Pattern> customValidators = [
            'protein': WILDCARD_PATTERN,
            'urineGlucose': WILDCARD_PATTERN,
            'urineBlood': WILDCARD_PATTERN,
            'urineNitrite': WILDCARD_PATTERN,
            'urineLeukocytes': WILDCARD_PATTERN
    ]

    @Override
    ConferenceMeasurementDraftType getType() {
        ConferenceMeasurementDraftType.URINE_COMBI
    }

    static Map<String, Closure> customConverters = [
            'protein': IDENTITY_CONVERTER,
            'urineGlucose': IDENTITY_CONVERTER,
            'urineBlood': IDENTITY_CONVERTER,
            'urineNitrite': IDENTITY_CONVERTER,
            'urineLeukocytes': IDENTITY_CONVERTER
    ]

    @Override
    List<String> getWarningFields() {
        []
    }
}
