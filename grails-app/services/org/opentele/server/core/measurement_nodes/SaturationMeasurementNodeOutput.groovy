package org.opentele.server.core.measurement_nodes

import org.opentele.server.core.QuestionnaireOutputBuilder
import org.opentele.server.core.QuestionnaireOutputElementBuilder
import org.opentele.server.core.model.types.DataType
import org.opentele.server.model.questionnaire.MeasurementNode
import org.springframework.context.i18n.LocaleContextHolder

class SaturationMeasurementNodeOutput implements MeasurementNodeOutput {

    // --*-- Fields --*--

    List<OutputVariable> outputVariables

    Closure mapToInputFieldsClosure, customClosure

    String nodeName, cancelVariableName, severityVariableName

    // --*-- Constructors --*--

    public SaturationMeasurementNodeOutput(node) {

        OutputVariable deviceId = new OutputVariable(
                "${node.id}.SAT#${MeasurementNode.DEVICE_ID_VAR}",
                DataType.STRING.value(), 'deviceId')

        OutputVariable saturation = new OutputVariable(
                "${node.id}.SAT#${MeasurementNode.SATURATION_VAR}",
                DataType.INTEGER.value(), 'saturation')

        OutputVariable pulse = new OutputVariable(
                "${node.id}.SAT#${MeasurementNode.PULSE_VAR}",
                DataType.INTEGER.value(), 'pulse')

        List variables = [
                deviceId, saturation, pulse
        ]

        Closure mapToInputFieldsClosure = { aNode, QuestionnaireOutputBuilder outputBuilder ->

            def elements = []
            elements << QuestionnaireOutputElementBuilder
                    .textViewElement(text: aNode.text)

            if (aNode.helpInfo) {
                elements << QuestionnaireOutputElementBuilder
                        .helpTextElement(text: aNode.helpInfo?.text,
                        imageFile: aNode.helpInfo?.helpImage?.id)
            }

            elements << QuestionnaireOutputElementBuilder
                    .textViewElement(text: outputBuilder.getMessageSource().getMessage(
                    'default.oxygen_saturation',
                    new String[0], LocaleContextHolder.locale))
            elements << QuestionnaireOutputElementBuilder
                    .editIntegerElement(outputBuilder.getOutputVariables(),
                    [variableName: "${node.id}.SAT#${MeasurementNode.SATURATION_VAR}"])

            elements << QuestionnaireOutputElementBuilder
                    .textViewElement(text: outputBuilder.getMessageSource().getMessage(
                    'default.pulse',
                    new String[0], LocaleContextHolder.locale))
            elements << QuestionnaireOutputElementBuilder
                    .editIntegerElement(outputBuilder.getOutputVariables(),
                    [variableName: "${node.id}.SAT#${MeasurementNode.PULSE_VAR}"])

            elements << outputBuilder.buttonsToSkipInputForNode(
                    rightNext: outputBuilder.defaultNextSeverityNodeName(aNode),
                    leftNext: outputBuilder.cancelNodeName(aNode))

            outputBuilder.addIoNode(nodeName: aNode.id,
                    nextNodeId: aNode.defaultNext.id,
                    elements: elements)
        }

        this.outputVariables = variables
        this.mapToInputFieldsClosure = mapToInputFieldsClosure
        this.customClosure = null
        this.nodeName = 'SaturationDeviceNode'
        this.cancelVariableName = "${node.id}.SAT##CANCEL"
        this.severityVariableName = "${node.id}.SAT##SEVERITY"
    }

    // --*-- Methods --*--

    @Override
    List<OutputVariable> getOutputVariables() {
        return outputVariables
    }

    @Override
    String getNodeName() {
        return nodeName
    }

    @Override
    String getCancelVariableName() {
        return cancelVariableName
    }

    @Override
    String getSeverityVariableName() {
        return severityVariableName
    }

    @Override
    Closure getMapToInputFieldsClosure() {
        return mapToInputFieldsClosure
    }

    @Override
    Closure getCustomClosure() {
        return customClosure
    }
}
