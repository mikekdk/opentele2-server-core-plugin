package org.opentele.server.core.measurement_nodes

interface MeasurementNodeOutput {

    List<OutputVariable> getOutputVariables()

    Closure getMapToInputFieldsClosure()
    Closure getCustomClosure()

    String getNodeName()
    String getCancelVariableName()
    String getSeverityVariableName()

}