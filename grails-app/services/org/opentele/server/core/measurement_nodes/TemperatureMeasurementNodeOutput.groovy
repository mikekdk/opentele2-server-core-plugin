package org.opentele.server.core.measurement_nodes

import org.opentele.server.core.QuestionnaireOutputBuilder
import org.opentele.server.core.QuestionnaireOutputElementBuilder
import org.opentele.server.core.model.types.DataType
import org.opentele.server.model.questionnaire.MeasurementNode

class TemperatureMeasurementNodeOutput implements MeasurementNodeOutput {

    // --*-- Fields --*--

    List<OutputVariable> outputVariables

    Closure mapToInputFieldsClosure, customClosure

    String nodeName, cancelVariableName, severityVariableName

    // --*-- Constructors --*--

    public TemperatureMeasurementNodeOutput(node) {

        OutputVariable temperature = new OutputVariable(
                "${node.id}.${MeasurementNode.TEMPERATURE_VAR}",
                DataType.FLOAT.value(), 'temperature')


        this.outputVariables = [temperature]
        this.mapToInputFieldsClosure = null
        this.customClosure = null
        this.nodeName = 'TemperatureDeviceNode'
        this.cancelVariableName = "${node.id}.${MeasurementNode.TEMPERATURE_VAR}#CANCEL"
        this.severityVariableName = "${node.id}.${MeasurementNode.TEMPERATURE_VAR}#SEVERITY"
    }

    // --*-- Methods --*--

    @Override
    List<OutputVariable> getOutputVariables() {
        return outputVariables
    }

    @Override
    String getNodeName() {
        return nodeName
    }

    @Override
    String getCancelVariableName() {
        return cancelVariableName
    }

    @Override
    String getSeverityVariableName() {
        return severityVariableName
    }

    @Override
    Closure getMapToInputFieldsClosure() {
        return mapToInputFieldsClosure
    }

    @Override
    Closure getCustomClosure() {
        return customClosure
    }
}
