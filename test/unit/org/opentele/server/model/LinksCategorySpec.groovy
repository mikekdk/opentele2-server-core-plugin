package org.opentele.server.model

import grails.buildtestdata.mixin.Build
import org.codehaus.groovy.grails.validation.MinSizeConstraint
import spock.lang.Specification

import static org.junit.Assert.*

import grails.test.mixin.*
import grails.test.mixin.support.*
@TestMixin(GrailsUnitTestMixin)
@Mock([LinksCategory, Link, PatientGroup])
@Build([PatientGroup])
class LinksCategorySpec extends Specification{

    def "when creating valid object no errors exist"() {
        given:
        def category = new LinksCategory(name: 'new category')
        category.addToLinks(new Link(title: 'some string', url: 'http://www.bt.dk'))
        category.addToPatientGroups(PatientGroup.build())

        when:
        def isValid = category.validate()

        then:
        isValid
        category.errors.errorCount == 0
    }

    def "category must have a name"() {
        given:
        def category = new LinksCategory()
        category.addToLinks(new Link(title: 'some string', url: 'http://www.bt.dk'))
        category.addToPatientGroups(PatientGroup.build())

        when:
        def isValid = category.validate()

        then:
        isValid == false
        category.errors.errorCount == 1
        category.errors.allErrors[0].field == 'name'
    }

    def "category must have at least one patient group"() {
        given:
        def category = new LinksCategory(name: 'new category')
        category.addToLinks(new Link(title: 'some string', url: 'http://www.bt.dk'))

        when:
        def isValid = category.validate()

        then:
        isValid == false
        category.errors.errorCount == 1
        category.errors.allErrors[0].field == 'patientGroups'
    }

    def "category must have at least one link"() {
        given:
        def category = new LinksCategory(name: 'new category')
        category.addToPatientGroups(PatientGroup.build())

        when:
        def isValid = category.validate()

        then:
        isValid == false
        category.errors.errorCount == 1
        category.errors.allErrors[0].field == 'links'
    }
}
