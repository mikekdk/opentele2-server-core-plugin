package org.opentele.server.core.exception

import org.springframework.validation.Errors

/**
 * Created with IntelliJ IDEA.
 * User: lch
 * Date: 7/3/12
 * Time: 11:15 AM
 * To change this template use File | Settings | File Templates.
 */
class EntityNotFoundException extends RuntimeException {
    Errors errors


    EntityNotFoundException() {
        super()
    }

    EntityNotFoundException(String s) {
        super(s)
    }


    EntityNotFoundException(String s, Errors errors) {
        super(s)
        this.errors = errors
    }
}
